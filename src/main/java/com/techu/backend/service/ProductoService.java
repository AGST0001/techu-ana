package com.techu.backend.service;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.model.UserModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductoService {

    // define una propiedad de la clase ProductoService que es de tipo lista de objetos de la clase ProductoModel
    // que la llama productoList y la crea vacía de tipo array

    private List<ProductoModel> productoList = new ArrayList<>();

    // definie un constructor ProductoService, sé que es un constructor porque no dice lo que
    // devuelve y sin recibir nada genera su propiedad productoList con 5 occurrencias
    public ProductoService() {
        productoList.add(new ProductoModel("1","producto 1",100.11));
        productoList.add(new ProductoModel("2","producto 2",200.22));
        productoList.add(new ProductoModel("3","producto 3",300.33));
        productoList.add(new ProductoModel("4","producto 4",400.44));
        productoList.add(new ProductoModel("5","producto 5",500.55));
        List<UserModel> users  = new ArrayList<>();
        users.add(new UserModel("1"));
        users.add(new UserModel("3"));
        users.add(new UserModel("5"));
        productoList.get(1).setUsers(users);
    }

    //READ productos
    public List<ProductoModel> getProductos() {
            return productoList;
        }

    //READ de un unico producto
    public ProductoModel getProductoById(String id) {
        int idx = Integer.parseInt(id);
        if (getIndex(idx) >=0)  {
            return productoList.get(getIndex(idx));
        }
        return null;
    }

    //CREATE productos - post
    public ProductoModel addProducto(ProductoModel nuevoProducto) {
        productoList.add(nuevoProducto);
        return nuevoProducto;
    }

    //UPDATE
    public ProductoModel updateProductobyId(int index, ProductoModel newPro) {
        int pos= getIndex(index);
        if (pos >0) {
            productoList.set(pos, newPro);
            return productoList.get(pos);
        }
        return null;
    //public ProductoModel updateProductobyId(int id, ProductoModel newPro) {
    //       Integer index = getIndex(id);
    //       productoList.set(index, newPro);
    //        return productoList.get(index);

    }

    //DELETE
    public void removeProductoById(int index) {
    //   int pos = productoList.indexOf(productoList.get(index -1));
        int pos = getIndex(index);
        if  (pos >= 0) {
                productoList.remove(pos);
        }
    //    public void removeProductoById(int id) {
    //        Integer index = getIndex(id);
    //        int pos = productoList.indexOf(productoList.get(index));
    //        productoList.remove(pos);
    }


    //Devuelve el indice o posicion de un producto en la colección de datos que tenemos en productoList
    public int getIndex(int index) {
        int i=0;
        while (i < productoList.size())  {
            if(Integer.parseInt(productoList.get(i).getId()) == index) {
                return (i);
            }
            i++;
        }
        return -1;
    }
    //UPDATE PATCH PRCIO
    public ProductoModel updateProductoPreciobyId(String index, double newPrecio) {
        int  idx = Integer.parseInt(index);
        int pos= getIndex(idx);

        if (pos >0) {
            ProductoModel pr = productoList.get(pos);
            pr.setPrecio(newPrecio);
            productoList.set(pos, pr);
            return productoList.get(pos);
            //productoList.get(pos).setPrecio(newPrecio);
        }
        return null;
        //public ProductoModel updateProductobyId(int id, ProductoModel newPro) {
        //       Integer index = getIndex(id);
        //       productoList.set(index, newPro);
        //        return productoList.get(index);

    }

}
