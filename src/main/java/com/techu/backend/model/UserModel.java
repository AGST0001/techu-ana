package com.techu.backend.model;

public class UserModel {
    private String id;
    private String nombre;
    private String rol;

    //constructor vació
    public UserModel() {

    }

    public  UserModel(String id)  {
        this.id = id;

    }

    //constructor

    public UserModel(String id, String nombre, String rol) {
        this.id = id;
        this.nombre = nombre;
        this.rol = rol;
    }

    //getter y setter

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
}
